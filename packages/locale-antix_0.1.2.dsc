Format: 3.0 (native)
Source: locale-antix
Binary: locale-antix
Architecture: all
Version: 0.1.2
Maintainer: nXecure <not@telling.you>
Homepage: https://gitlab.com/antix-contribs/locale-antix
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 10), gettext, bash, coreutils, util-linux, sed
Package-List:
 locale-antix deb localization optional arch=all
Checksums-Sha1:
 1ec23c1e11fd84758276ba29b4530d578a80443d 293440 locale-antix_0.1.2.tar.xz
Checksums-Sha256:
 d4dd139a0276f00678b494a98223e39ae093437463c6750025c7962a440f621e 293440 locale-antix_0.1.2.tar.xz
Files:
 79d2f6c7f1df17f9b113c587325c63c8 293440 locale-antix_0.1.2.tar.xz
